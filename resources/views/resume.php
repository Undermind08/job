<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="../css/undermind2.css" />
</head>
<body>
<header class="header-banner">
  <div class="container-width">
  <div class="block-3">
  <img  class="img_profile" src="http://hareruyaa.com/img/app.png">
    </div>
    <div class="logo-container">
      <!-- <div class="logo">วัทธิกร ประจงบัว
      </div> -->
    </div>
    <div>
 
    </div>
    <div class="clearfix">
    </div>
    <div class="lead-title">VATTIKORN PRAJONKBUA
    </div>
    <div class="lead-title2">วัทธิกร ประจงบัว
    </div>
    <div class="lead-title3">Web Developer
    </div>
    <div class="sub-lead-title"> Tel : 0808473436,  Email: vattikornnook@gmail.com,Line ID: nukundermind, 
        ที่อยู่:บ้านอุ่นเกล้าเพลส ห้อง 207 96ถ.เจริญสุข ต.ช้างเผือก เมืองเชียงใหม่, เชียงใหม่ 50300
    </div>
  </div>
</header>
<section class="flex-sect">
  <div class="container-width">
    <div class="flex-title">รายละเอียด
    </div>
    <!-- <div class="flex-desc">With flexbox system you're able to build complex layouts easily and with free responsivity
    </div> -->

    <div class="row" id="i6zj3e">
      <div class="cell" id="i6pwks">
          
      <div class="badge-body">
      <nav class="navbar navbar-inverse">
      <ul class="nav navbar-nav">
          <div class="lead-btn">เป้าหมายการทำงาน
          </div>
    </ul>
</nav>
          <div class="badge-desc">เพื่อประสบความสำเร็จในวิชาชีพและต้องการใช้ความรู้ทักษะเรียนรู้ การทำงานขององค์กรเพื่อพัฒนาศักยภาพ มีความสนใจในเทคโนโลยีพร้อมเรียนรู้และเปิดรับสิ่งใหม่ๆอยู่เสมอ
          </div>
        </div>

          <div class="badge-body2">
          <ul class="nav navbar-nav">
          <div class="lead-btn" style="margin-top:20px;">การศึกษา
          </div>
          </ul>
          <div class="badge-desc"><b>ชั้นอนุบาล</b>: โรงเรียนวัฒนวิทยา
          </div>
          <div class="badge-desc"><b>ประถมศึกษา</b>: โรงเรียนสัญลักษณ์วิทยา
          </div>
          <div class="badge-desc"><b>มัทธยมศึกษาตอนต้น</b>: โรงเรียนประชานุสรณ์วิทยา
          </div>
          <div class="badge-desc"><b>ปัจจุบัน</b>: มหาวิทยาลัยราชภัฏเชียงใหม่ <b>คณะ</b>วิทยาศาสตร์ 
</b>สาขา</b>เทคโนโลยีสาระสนเทศ <b>(Information Technology)</b>
          </div>
          <div class="badge-desc"><b>มัทธยมศึกษาตอนปลาย</b>: โรงเรียนปากช่อง
          </div>
        </div>

        <div class="badge-body">
        <ul class="nav navbar-nav">
          <div class="lead-btn" style="margin-top:20px;">งานที่ต้องการ
          </div>
</ul>
          <div class="badge-desc"><b>ประเภทของงาน:</b> งานประจำ, งานชั่วคราว, งานตามสัญญาจ้าง รายเดือน/รายปี, งานอิสระ
          </div>
          <div class="badge-desc"><b>สายงาน:</b> ไอที / คอมพิวเตอร์: โปรแกรมเมอร์เว็บไซต์
          </div>
          <div class="badge-desc"><b>พื้นที่ทำงาน:</b> เชียงใหม่
          </div>
          <div class="badge-desc"><b>สามารถเริ่มงานใหม่ได้ตั้งแต่:</b> 7 วัน
          </div>
        </div>

              <div class="badge-body">
              <ul class="nav navbar-nav">
             <div class="lead-btn"  style="margin-top:20px;">ประสบการณ์การทำงาน
             </div>
            </ul>
          <div class="badge-desc"><b>ประสบการณ์การทำงานทั้งหมด:</b> 5 เดือน, 1 บริษัท
          </div>
          <div class="badge-desc"><b>บริษัท:</b> Living-Intertrade
          </div>
          <div class="badge-desc"><b>ประเภทของธุรกิจ:</b> การค้า/นำเข้า/ส่งออก
          </div>
          <div class="badge-desc"><b>ตำแหน่งล่าสุด:</b> งานออกแบบWeb/UI/UX
          </div>
          <div class="badge-desc"><b>ความรับผิดชอบ:</b> สร้างและดูและ Landing Page, Report ของบริษัท
          </div>
      </div>

       <div class="badge-body">
               <ul class="nav navbar-nav">
          <div class="lead-btn" style="margin-top:20px;">ประสบการณ์การฝึกงาน
          </div>
</ul>
          <div class="badge-desc"><b>ประสบการณ์การฝึกงานทั้งหมด:</b> 3 เดือน, 1 บริษัท
          </div>
          <div class="badge-desc"><b>บริษัท:</b> ธาราอีซูซุ เชียงใหม่
          </div>
          <div class="badge-desc"><b>ประเภทของธุรกิจ:</b> จัดจำหน่ายรถยนต์และอะไหล่
          </div>
          <div class="badge-desc"><b>ตำแหน่งล่าสุด:</b> โปรแกรมเมอร์
          </div>
          <div class="badge-desc"><b>ความรับผิดชอบ:</b> สร้างเว็บไซต์ระบบเคาะพ่นสีของบริษัท
          </div>
        </div>
        
      </div>
      <div class="cell" id="ivf2q6">
     

         

          
          <div class="badge-body">
          <ul class="nav navbar-nav">
          <div class="lead-btn" style="margin-top:20px;">ทักษะและภาษา
          </div>
          </ul>
          <div class="badge-desc"><b>ภาษา:อังกฤษ</b>
          </div>
          <div class="badge-desc" style="margin-top:;"><b>อ่าน &nbsp;&nbsp;:</b>
           <img src="http://hareruyaa.com/img/Yellow_Star_opt.png" alt="" style="width:15px; height:15px;">
           <img src="http://hareruyaa.com/img/Yellow_Star_opt.png" alt="" width="15" height="15">
           <img src="http://hareruyaa.com/img/Yellow_Star_opt.png" alt="" width="15" height="15">
           <img src="http://hareruyaa.com/img/Yellow_Star_opt.png" alt="" width="15" height="15">
          </div>

          <div class="badge-desc" style="margin-top:;"><b>ฟัง&nbsp;&nbsp; &nbsp; :</b>
           <img src="http://hareruyaa.com/img/Yellow_Star_opt.png" alt="" style="width:15px; height:15px;">
           <img src="http://hareruyaa.com/img/Yellow_Star_opt.png" alt="" width="15" height="15">

          </div>

           <div class="badge-desc" style="margin-top:;"><b>เขียน :</b>
           <img src="http://hareruyaa.com/img/Yellow_Star_opt.png" alt="" style="width:15px; height:15px;">
           <img src="http://hareruyaa.com/img/Yellow_Star_opt.png" alt="" width="15" height="15">
           <img src="http://hareruyaa.com/img/Yellow_Star_opt.png" alt="" width="15" height="15">

          </div>

            <div class="badge-desc" style="margin-top:;"><b>พูด &nbsp;&nbsp;&nbsp;:</b>
           <img src="http://hareruyaa.com/img/Yellow_Star_opt.png" alt="" style="width:15px; height:15px;">
           <img src="http://hareruyaa.com/img/Yellow_Star_opt.png" alt="" width="15" height="15">
           <img src="http://hareruyaa.com/img/Yellow_Star_opt.png" alt="" width="15" height="15">
           <img src="http://hareruyaa.com/img/Yellow_Star_opt.png" alt="" width="15" height="15">
          </div>
          <div class="badge-desc"><b>ประเภทของธุรกิจ:</b> จัดจำหน่ายรถยนต์และอะไหล่
          </div>
          <div class="badge-desc"><b>ตำแหน่งล่าสุด:</b> โปรแกรมเมอร์
          </div>
          <div class="badge-desc"><b>ความรับผิดชอบ:</b> สร้างเว็บไซต์ระบบเคาะพ่นสีของบริษัท
          </div>
        </div>
        <div class="badge-body">
        <ul class="nav navbar-nav">
          <div class="lead-btn" style="margin-top:20px;">Computer Skill
          </div>
        </ul>
          <div class="badge-desc"><b>Internet Of Things(IOT) :</b>
           <img src="http://hareruyaa.com/img/Yellow_Star_opt.png" alt="" style="width:15px; height:15px;">
           <img src="http://hareruyaa.com/img/Yellow_Star_opt.png" alt="" style="width:15px; height:15px;">
          </div>
          <div class="badge-desc"><b>Photoshop: </b> 
          <img src="http://hareruyaa.com/img/Yellow_Star_opt.png" alt="" style="width:15px; height:15px;">
          <img src="http://hareruyaa.com/img/Yellow_Star_opt.png" alt="" style="width:15px; height:15px;">
          <img src="http://hareruyaa.com/img/Yellow_Star_opt.png" alt="" style="width:15px; height:15px;">
          </div>
          <div class="badge-desc"><b>Java:</b> 
          <img src="http://hareruyaa.com/img/Yellow_Star_opt.png" alt="" style="width:15px; height:15px;">
          </div>
          <div class="badge-desc"><b>PHP:</b>
          <img src="http://hareruyaa.com/img/Yellow_Star_opt.png" alt="" style="width:15px; height:15px;">
          <img src="http://hareruyaa.com/img/Yellow_Star_opt.png" alt="" style="width:15px; height:15px;">
          <img src="http://hareruyaa.com/img/Yellow_Star_opt.png" alt="" style="width:15px; height:15px;">
          </div>
          <div class="badge-desc"><b>SQL:</b>
          <img src="http://hareruyaa.com/img/Yellow_Star_opt.png" alt="" style="width:15px; height:15px;">
          <img src="http://hareruyaa.com/img/Yellow_Star_opt.png" alt="" style="width:15px; height:15px;">
    
          </div>
          <div class="badge-desc"><b>CSS:</b> 
          <img src="http://hareruyaa.com/img/Yellow_Star_opt.png" alt="" style="width:15px; height:15px;">
          <img src="http://hareruyaa.com/img/Yellow_Star_opt.png" alt="" style="width:15px; height:15px;">
          <img src="http://hareruyaa.com/img/Yellow_Star_opt.png" alt="" style="width:15px; height:15px;">
          </div>
          <div class="badge-desc"><b>HTML:</b> 
          <img src="http://hareruyaa.com/img/Yellow_Star_opt.png" alt="" style="width:15px; height:15px;">
          <img src="http://hareruyaa.com/img/Yellow_Star_opt.png" alt="" style="width:15px; height:15px;">
          <img src="http://hareruyaa.com/img/Yellow_Star_opt.png" alt="" style="width:15px; height:15px;">
          <img src="http://hareruyaa.com/img/Yellow_Star_opt.png" alt="" style="width:15px; height:15px;">
          </div>
        </div>
        
      </div>
    </div>

    <div class="cards">
    </div>
  </div>
</section>
<section class="am-sect">
  <div class="container-width">

  </div>
</section>
<section class="blk-sect">
  <div class="container-width">

  </div>
</section>
<!-- <section class="bdg-sect">
  <div class="container-width">
    <h1 class="bdg-title">The team
    </h1>
    <div class="badges">
      <div class="badge">
        <div class="badge-header">
        </div>
        <img src="img/team1.jpg" class="badge-avatar"/>
        <div class="badge-body">
          <div class="badge-name">Adam Smith
          </div>
          <div class="badge-role">CEO
          </div>
          <div class="badge-desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore ipsum dolor sit
          </div>
        </div>
        <div class="badge-foot">
          <span class="badge-link">f</span>
          <span class="badge-link">t</span>
          <span class="badge-link">ln</span>
        </div>
      </div>
      <div class="badge">
        <div class="badge-header">
        </div>
        <img src="img/team2.jpg" class="badge-avatar"/>
        <div class="badge-body">
          <div class="badge-name">John Black
          </div>
          <div class="badge-role">Software Engineer
          </div>
          <div class="badge-desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore ipsum dolor sit
          </div>
        </div>
        <div class="badge-foot">
          <span class="badge-link">f</span>
          <span class="badge-link">t</span>
          <span class="badge-link">ln</span>
        </div>
      </div>
      <div class="badge">
        <div class="badge-header">
        </div>
        <img src="img/team3.jpg" class="badge-avatar"/>
        <div class="badge-body">
          <div class="badge-name">Jessica White
          </div>
          <div class="badge-role">Web Designer
          </div>
          <div class="badge-desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore ipsum dolor sit
          </div>
        </div>
        <div class="badge-foot">
          <span class="badge-link">f</span>
          <span class="badge-link">t</span>
          <span class="badge-link">ln</span>
        </div>
      </div>
    </div>
  </div>
</section>
<footer class="footer-under">
  <div class="container-width">
    <div class="footer-container">
      <div class="foot-lists">
        <div class="foot-list">
          <div class="foot-list-title">About us
          </div>
          <div class="foot-list-item">Contact
          </div>
          <div class="foot-list-item">Events
          </div>
          <div class="foot-list-item">Company
          </div>
          <div class="foot-list-item">Jobs
          </div>
          <div class="foot-list-item">Blog
          </div>
        </div>
        <div class="foot-list">
          <div class="foot-list-title">Services
          </div>
          <div class="foot-list-item">Education
          </div>
          <div class="foot-list-item">Partner
          </div>
          <div class="foot-list-item">Community
          </div>
          <div class="foot-list-item">Forum
          </div>
          <div class="foot-list-item">Download
          </div>
          <div class="foot-list-item">Upgrade
          </div>
        </div>
        <div class="clearfix">
        </div>
      </div>
      <div class="form-sub">
        <div class="foot-form-cont">
          <div class="foot-form-title">Subscribe
          </div>
          <div class="foot-form-desc">Subscribe to our newsletter to receive exclusive offers and the latest news
          </div>
          <input name="name" placeholder="Name" class="sub-input"/>
          <input name="email" placeholder="Email" class="sub-input"/>
          <button type="button" class="sub-btn">Submit</button>
        </div>
      </div>
    </div>
  </div>
  <div class="copyright">
    <div class="container-width">
      <div class="made-with">
        made with GrapesJS
      </div>
      <div class="foot-social-btns">facebook twitter linkedin mail
      </div>
      <div class="clearfix">
      </div>
    </div>
  </div>
</footer> -->
</body>
</html>