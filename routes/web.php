<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/01', function () {
    return view('home');
});

Route::get('/02', function () {
    return view('product');
});

Route::get('/03', function () {
    return view('test');
});

Route::get('/04', function () {
    return view('resume');
});